<?php

namespace ChadoSearch\form\combo;

use ChadoSearch\Set;

class RepeatableText extends Filter {
  
  public $required;
  public $size;
  public $items;
  public $itemTypes;
  public $maxlength;
  public $autocomplete_path;
  
  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    $id_select = $id . '_select';
    $id_op = $id . '_op';
    $id_text = $id . '_text';
    $id_andor = $id . '_andor';
    $id_add = $id . '_add';
    $id_remove = $id . '_remove';

    $counter = isset($form_state['storage'][$id . '-counter']) ? $form_state['storage'][$id . '-counter'] : 1;
    // If Clear button is clicked to clear all values
    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == 'chado_search-id-clear-all-values' && $form_state['triggering_element']['#type'] == 'button') {
        $counter = 1;
        $form_state['storage'][$id . '-counter'] = 1;
    }
    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == 'chado_search-id-' . $id_add) {
      $counter ++;
      $form_state['storage'][$id . '-counter'] = $counter;
    }
    else if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == 'chado_search-id-' . $id_remove) {
      if ($counter > 1) {
        $counter --;
        $form_state['storage'][$id . '-counter'] = $counter;
      }
    }
    
    // create a container to hold widgets
    $form[$id] = array(
      '#type' => 'container',
      '#prefix' => "<div id=\"chado_search-filter-$search_name-$id\" class=\"chado_search-filter-group chado_search-filter-field chado_search-widget\">",
      '#suffix' => "</div>"
    );
    
    for ($i = 0; $i < $counter; $i ++) {
      // Create widgets
      $this->csform->addSelect(Set::select()->id($id_select . '-' . $i)->options(array_merge(array(0 => 'Select a field'), $this->items)));
      // If $itemType is set, dynamically populate the $id_op select box
      if (count($this->itemTypes) > 0) {
        $selected = isset($form_state['values'][$id_select . '-' . $i]) ? $form_state['values'][$id_select . '-' . $i] : 0;
        $operators = array('-');
        if ($selected) {
          $type = $this->itemTypes[$selected];
          if (in_array($type, array('smallint', 'integer', 'bigint', 'decimal', 'numeric', 'real', 'double precision', 'smallserial', 'serial', 'bigserial'))) {
            $operators = array('>' => '>', '=' => '=', '<' => '<');
          } else if ($type == 'boolean') {
            $operators = array('false' => 'False', 'true' => 'True');
          } else if (in_array($type, array('character varying', 'varchar', 'character', 'char', 'text'))) {
            $operators = array ('contains' => 'contains', 'exactly' => 'exactly', 'starts' => 'starts with', 'ends' => 'ends with', 'not_contain' => 'does not contain');
          }
        }
        // If Clear button is clicked to clear all values
        if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == 'chado_search-id-clear-all-values' && $form_state['triggering_element']['#type'] == 'button') {
            $operators = array('-');
            $form_state['values'][$id_select . '-' . $i] = 0;
            $form_state['input'][$id_select . '-' . $i] = 0;
        }
        $this->csform->addSelect(Set::select()->id($id_op . '-' . $i)->options($operators));
        $form[$id_select . '-' . $i]['#ajax'] = array(
            'callback' => 'chado_search_ajax_form_update_repeatable_text_dynamic_types',
            'wrapper' => 'chado_search-id-' . $id . '_ot_wrapper-' . $i,
            'effect' => 'fade'
        );
        // Add classes
        $form[$id_select . '-' . $i]['#attributes'] = array(
          'class' => array('chado_search-repeatable_text-' . $search_name . '-select', 'chado_search-repeatable_text-select')
        );
        $form[$id_op . '-' . $i]['#attributes'] = array(
          'class' => array('chado_search-repeatable_text-' . $search_name . '-operator', 'chado_search-repeatable_text-operator')
        );
      }
      // Statically populate $id_op if $itemType is not set
      else {
          $this->csform->addSelect(Set::select()->id($id_op . '-' . $i)->options(array ('contains' => 'contains', 'exactly' => 'exactly', 'starts' => 'starts with', 'ends' => 'ends with', 'not_contain' => 'does not contain')));
      }
      // Add textfield
      $text_setting = Set::textField()->id($id_text . '-' . $i)->required($this->required)->size($this->size)->maxlength($this->maxlength);
      if ($selected) {
        $text_setting = $text_setting->autoCompletePath($this->autocomplete_path . '/' . $selected);
      }
      $this->csform->addTextfield($text_setting);
      // If Clear button is clicked to clear all values
      if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == 'chado_search-id-clear-all-values' && $form_state['triggering_element']['#type'] == 'button') {
          $form[$id_text . '-' . $i]['#value'] = '';
      }
      $form[$id_text . '-' . $i]['#attributes'] = array(
        'class' => array('chado_search-repeatable_text-' . $search_name . '-text', 'chado_search-repeatable_text-text')
      );
      // Add AND/OR selector
      if ($counter - $i > 1) {
        $this->csform->addSelect(Set::select()->id($id_andor . '-' . $i)->options(array ('and' => 'AND', 'or' => 'OR')));
        if ($i != $counter - 1) {
          $form[$id_andor . '-' . $i]['#suffix'] .= "<div class=\"chado_search-element-newline\"> </div>";
        }
      }
      else {
        // Insert newline
        if ($i != $counter - 1) {
          $form[$id_text . '-' . $i]['#suffix'] .= "<div class=\"chado_search-element-newline\"> </div>";
        }
      }
      // Move everyting into the container
      $form[$id][$id_select . '-' . $i] = $form[$id_select . '-' . $i];
      // Create an op_text container
      $id_ot_wrapper = $id . '_ot_wrapper-' . $i;
      $form[$id][$id_ot_wrapper] = array(
        '#type' => 'container',
        '#prefix' => '<div id="chado_search-id-' . $id . '_ot_wrapper-' . $i . '" class="chado_search-widget">',
        '#suffix' => '</div>',
      );
      $form[$id][$id_ot_wrapper][$id_op . '-' . $i] = $form[$id_op . '-' . $i];
      $form[$id][$id_ot_wrapper][$id_text . '-' . $i] = $form[$id_text . '-' . $i];
      unset($form[$id_select . '-' . $i]);
      unset($form[$id_op . '-' . $i]);
      unset($form[$id_text . '-' . $i]);      
      if (isset($form[$id_andor . '-' . $i])) {
        $form[$id][$id_andor . '-' . $i] = $form[$id_andor . '-' . $i];
        unset($form[$id_andor . '-' . $i]);
      }
    }

    // Add button
    $this->csform->addButton(Set::button()->id($id_add)->value('Add'));
    $form[$id][$id_add] = $form[$id_add];
    $form[$id][$id_add]['#attributes'] = array('class' => array('form-item'));
    $form[$id][$id_add]['#ajax'] = array(
      'callback' => 'chado_search_ajax_form_update_repeatable_text',
      'wrapper' => "chado_search-filter-$search_name-$id",
      'effect' => 'fade'
    );
    unset($form[$id_add]);
    
    // Remove button
    if ($counter > 1) {
      $this->csform->addButton(Set::button()->id($id_remove)->value('Remove'));
      $form[$id][$id_remove] = $form[$id_remove];
      $form[$id][$id_remove]['#attributes'] = array('class' => array('form-item'));
      $form[$id][$id_remove]['#ajax'] = array(
        'callback' => 'chado_search_ajax_form_update_repeatable_text',
        'wrapper' => "chado_search-filter-$search_name-$id",
        'effect' => 'fade'
      );
      unset($form[$id_remove]);
    }
  }
}