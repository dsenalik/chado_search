<?php

namespace ChadoSearch\form\combo;

class Filter {
  
  public $id;
  public $csform;
  public $search_name;
  public $path;
  public $newline;
  public $fieldset_id;
  public $default_value;
  public $value;
  public $ajax;
  
  public function __construct($csform) {
    $this->csform = $csform;
    $this->search_name = $csform->search_name;
    $this->path = $csform->path;
  }
  
    // Allow the sub-class to override or add new attributes to the form
  // If the subclass has its own veriable(s) to be used to set the form, you have to
  // use this function to add those changes
  public function setForm (&$form, &$form_state) {}
  
  public function attach(&$form, &$form_state) {

    // Warn if ID is not specified
    if (!$this->id) {
      form_set_error('invalid_id', "Please specify an ID for the'" . chado_search_get_class($this) . "'.");
    }
    
    // Allow override or add more attributes
    $this->setForm ($form, $form_state);
    
    // Add a new line at the end of the filter widget
    if ($this->newline) {
      $id = $this->id;
      if(chado_search_get_class($this) == 'BetweenFilter') {
        $id = $this->id2;
      }
      $form[$id]['#suffix'] .= "<div class=\"chado_search-element-newline\"> </div>";
    }
    
    // Move the filter into a fieldset if specified
    if ($this->fieldset_id) {
      $fieldset_element = &find_first_form_element_by_id($form, $this->fieldset_id);
      if ($fieldset_element && $fieldset_element['#type'] == 'fieldset') {
        if (isset($form[$this->id . '_label'])) {
          $fieldset_element[$this->id . '_label'] = $form[$this->id . '_label'];
          unset($form[$this->id . '_label']);
        }
        if (isset($form[$this->id . '_op'])) {
          $fieldset_element[$this->id . '_op'] = $form[$this->id . '_op'];
          unset($form[$this->id . '_op']);
        }
        if (isset($form[$this->id])) {
          $fieldset_element[$this->id] = $form[$this->id];
          unset($form[$this->id]);
        }
        if (property_exists($this, 'id2') && isset($form[$this->id2])) {
          $fieldset_element[$this->id2] = $form[$this->id2];
          unset($form[$this->id2]);
        }
        $form_state['input'][$this->id] = $this->value ? $this->value : $this->default_value;
      }
      else {
        form_set_error('invalid_fieldset_id', "To add an element to a fieldset, please ensure the fieldset was created before the " . chado_search_get_class($this) . " element.");
      }
    }
  }
}