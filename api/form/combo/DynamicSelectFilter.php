<?php

namespace ChadoSearch\form\combo;

use ChadoSearch\Set;

class DynamicSelectFilter extends Filter {
  
  public $title;
  public $depend_on_id;
  public $callback;
  public $size;
  public $cacheTable;
  public $cacheColumns;
  public $reset_on_change_id;
  public $multiple;
  public $alsoDependOn;
  public $hidden;
  
  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    
    $tname = "cache_" . $search_name . '_' . $id;
    $tname = substr($tname, 0, 63);
    // Create cache table if specified
    if ($this->cacheTable && count($this->cacheColumns) > 0) {
      $cols = '';
      $counter = 0;
      foreach ($this->cacheColumns AS $col) {
        $cols .= $col;
        if ($counter < count ($this->cacheColumns) - 1) {
          $cols .= ', ';
        }
        $counter ++;
      }
      
      if (!db_table_exists($tname)) { // If the cache table not exists, create it
        chado_search_query("SELECT distinct $cols INTO $tname FROM {" . $this->cacheTable . "}");
        $lastupdate = db_query(
            "SELECT last_update FROM tripal_mviews WHERE mv_table = :mv_table",
            array(':mv_table' => $this->cacheTable)
            )->fetchField();
            variable_set("chado_search_cache_last_update_" . $tname, $lastupdate);
      } else { // If cache table exists, check if it's up-to-date. If it's not up-to-date, recreate the cache table
        $lastupdate = db_query(
            "SELECT last_update FROM tripal_mviews WHERE mv_table = :mv_table",
            array(':mv_table' => $this->cacheTable)
            )->fetchField();
            if (variable_get("chado_search_cache_last_update_" . $tname, "") != $lastupdate) {
              chado_search_query("DROP table $tname");
              chado_search_query("SELECT distinct $cols INTO $tname FROM {" . $this->cacheTable . "}");
              variable_set("chado_search_cache_last_update_" . $tname, $lastupdate);
            }
      }
    }
    // Remove the cache table if cache not specified
    else {
      if (db_table_exists($tname)) {
        chado_search_query("DROP table $tname");
        variable_del("chado_search_cache_last_update_" . $tname);
      }
    }
    
    $id_label = $id . '_label';
    $title = $this->title;
    $depend_on_id = $this->depend_on_id;
    $multiple = $this->multiple;
    $alsoDependOn = $this->alsoDependOn;
    $width = '';
    if ($this->label_width) {
      $width = "width:" . $this->label_width ."px";
    }
    $size = $this->size;
    $depend_on_element = &find_first_form_element_by_id($form, $depend_on_id);
    // Add Ajax to the depending element
    $selected = isset($form_state['values'][$depend_on_id]) ? $form_state['values'][$depend_on_id] : 0;
    $depend_on_element['#ajax'] = array(
      //'callback' => 'chado_search_ajax_form_update', // deprecated. drupal won't allow multiple selection when a file upload exists
      'path' => 'chado_search_ajax_callback',
      'wrapper' => "chado_search-filter-$search_name-$id-field",
      'effect' => 'fade'
    );
    if(isset($depend_on_element['#attribute']['update'])) {
      $updates = $depend_on_element['#attribute']['update'];
      if (!is_array($updates)) {
        $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
      }
      $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
      $depend_on_element['#attribute'] = array ('update' => $updates);
    }
    else {
      $depend_on_element['#attribute'] = array ('update' => $id);
    }

    // Also add Ajax to other dependent elements
    foreach ($alsoDependOn AS $did) {
      $did_element = &find_first_form_element_by_id($form, $did);
      $did_element['#ajax'] = array(
        //'callback' => 'chado_search_ajax_form_update', // deprecated. drupal won't allow multiple selection when a file upload exists
        'path' => 'chado_search_ajax_callback',
        'wrapper' => "chado_search-filter-$search_name-$id-field",
        'effect' => 'fade'
      );
      if(isset($did_element['#attribute']['update'])) {
        $updates = $did_element['#attribute']['update'];
        if (!is_array($updates)) {
          $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
        }
        $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
        $did_element['#attribute'] = array ('update' => $updates);
      }
      else {
        $did_element['#attribute'] = array ('update' => $id);
      }
    }
    
    // Add Ajax to reset values on change of another element
    $reset_on_change_id = $this->reset_on_change_id;
    $reset_on_element = &find_first_form_element_by_id($form, $reset_on_change_id);
    if ($reset_on_change_id) {
      unset($form_state['values'][$id]);
      $updates = $reset_on_element['#attribute']['update'];
      if (!is_array($updates)) {
        $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
      }
      $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
      $reset_on_element['#attribute'] = array ('update' => $updates);
    }
    
    $callback = $this->callback;
    $opt = $callback($selected, $form, $form_state);
    $display = 'display:block';
    $js = '';
    $num_opt = key_exists(0, $opt) && in_array('Any', $opt) ? count($opt) - 1 : count($opt);
    if ($num_opt > 0 || !$this->hidden) {
        $js = 
          "<script type=\"text/javascript\">
            (function ($) {
              $(document).ready(function(){
                $('#chado_search-filter-$search_name-$id-label').show();
                $('#chado_search-filter-$search_name-$id-field').show();
              });
            })(jQuery);
           </script>";
    }
    else {
        $display = 'display:none';
        $js =
        "<script type=\"text/javascript\">
            (function ($) {
              $(document).ready(function(){
                $('#chado_search-filter-$search_name-$id-label').hide();
                $('#chado_search-filter-$search_name-$id-field').hide();
              });
            })(jQuery);
           </script>";
    }
    
    // Add Label
    $this->csform->addMarkup(Set::markup()->id($id_label)->text($title));
    $form[$id_label]['#prefix'] =
      "<div id=\"chado_search-filter-$search_name-$id-label\" class=\"chado_search-filter-label form-item\" style=\"$display;$width\">";
    $form[$id_label]['#suffix'] =
      "</div>";
    // Add Select
    // If Clear button is clicked to clear all values
    if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == 'chado_search-id-clear-all-values' && $form_state['triggering_element']['#type'] == 'button') {
        $opt = array (0 => 'Any');
    }
    if (!$opt) {
      $opt = array (0 => 'Any');
    }
    if (function_exists($callback)) {
      //$selected_value = is_array($selected) ? array_shift($selected) : $selected; //deprecated. this only allows one selection
      $this->csform->addSelect(Set::select()->id($id)->options($opt)->multiple($multiple)->size($size));
      $form[$id]['#prefix'] =
        "<div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter-field chado_search-widget\"><div style=\"$display\">$js";
      $form[$id]['#suffix'] =
        "</div></div>";
    }
    else {
      drupal_set_message('Fatal Error: DynamicSelectFilter ajax function not implemented', 'error');
    }
  }
}
