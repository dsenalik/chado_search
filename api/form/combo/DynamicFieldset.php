<?php

namespace ChadoSearch\form\combo;

class DynamicFieldset extends Filter {
  
  public $depend_on_id;
  public $title;
  public $description;
  public $collapsible;
  public $collapsed;
  public $width;
  public $display;
  
  public function setForm (&$form, &$form_state) {
    $search_name = $this->search_name;
    $id = $this->id;
    $depend_on_id = $this->depend_on_id;
    $title = $this->title;
    $desc = $this->description;
    $collapsible = $this->collapsible;
    $collapsed = $this->collapsed;
    $style = 'style="';   
    if ($this->width) {
      if (preg_match('/\d+%$/', $this->width)) {
        $style .=  "width:" . $this->width . ";";
      }
      else {
        $style .=  "width:" . $this->width . "px" . ";";
      }
    }
    
    if ($this->display) {
      $style .= 'display:' . $this->display;
    }
    $style .= '"';
    $js = '';
    if ($this->display) {
      $js = 
        '<script type=\"text/javascript\">
          (function ($) {
             $(document).ready(
               function() {
                 $(\'#chado_search-filter-' . "$search_name-$id" . '-field\').css("display","' . $this->display . '");
               }
             )
          })(jQuery)</script>';
    }
    
    $depend_on_element = &find_first_form_element_by_id($form, $depend_on_id);
    
    // Add Ajax to the depending element
    $depend_on_element['#ajax'] = array(
      'path' => 'chado_search_ajax_callback',
      'wrapper' => "chado_search-filter-$search_name-$id-field",
      'effect' => 'fade'
    );
    if(isset($depend_on_element['#attribute']['update'])) {
      $updates = $depend_on_element['#attribute']['update'];
      if (!is_array($updates)) {
        $updates = array($updates => array('wrapper' => "chado_search-filter-$search_name-$updates-field"));
      }
      $updates[$id] = array('wrapper' => "chado_search-filter-$search_name-$id-field");
      $depend_on_element['#attribute'] = array ('update' => $updates);
    }
    else {
      $depend_on_element['#attribute'] = array ('update' => $id);
    }
      
    $form [$id] = array(
      '#id' => 'chado_search-id-' . $id,
      '#type' => 'fieldset',
      '#title' => $title,
      '#description' => $desc,
      '#collapsible' => $collapsible,
      '#collapsed' => $collapsed,
      '#prefix' => "$js<div id=\"chado_search-filter-$search_name-$id-field\" class=\"chado_search-filter chado_search-widget form-item\" $style>",
      '#suffix' => "</div>"
    );
  }
}