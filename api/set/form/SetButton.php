<?php

namespace ChadoSearch\set\form;

class SetButton extends SetBase {

  private $value = 'Apply';
  private $type = 'button';
  private $name = 'button';
  
  /**
   * Setters
   * @return $this
   */
  public function value ($value) {
    $this->value = $value;
    return $this;
  }
  
  public function name ($name) {
    $this->name = $name;
    return $this;
  }

  public function buttonType ($type) {
    $this->type = $type;
    return $this;
  }
  
  /**
   * Getters
   */
  public function getValue () {
    return $this->value;
  }
  
  public function getName () {
    return $this->name;
  }
 
  public function getType () {
    return $this->type;
  }
  
}