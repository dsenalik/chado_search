<?php

namespace ChadoSearch\set\form;

class SetSubmit extends SetBase {

  private $value = NULL;
  private $name = 'submit_button';
  
  /**
   * Setters
   * @return $this
   */
  public function value ($value) {
    $this->value = $value;
    return $this;
  }
  
  public function name ($name) {
    $this->name = $name;
    return $this;
  }
  
  
  /**
   * Getters
   */
  public function getValue () {
    return $this->value;
  }
  
  public function getName () {
    return $this->name;
  }
}