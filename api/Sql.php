<?php
namespace ChadoSearch;

use ChadoSearch\sql\AggregateColumnCond;
use ChadoSearch\sql\BetweenCond;
use ChadoSearch\sql\ColumnCond;
use ChadoSearch\sql\FileCond;
use ChadoSearch\sql\FileMultiColumnsCond;
use ChadoSearch\sql\MultiColumnsCond;
use ChadoSearch\sql\SelectCond;
use ChadoSearch\sql\RepeatableTextCond;
use ChadoSearch\sql\Statement;

/**
 * A class to help generate SQL for form elements
 *
 */

class Sql {

  /****************************************************
   * SQL statement builders
   */
  
  // SQL for LabeledFilter
  static function labeledFilter ($filter_id, &$form_state, $column, $case_sensitive = FALSE, $op = 'exactly', $ignore_space = TRUE) {
    $value = $form_state['values'][$filter_id];
    $s = new ColumnCond($column, $op, $value, $case_sensitive, NULL, 'OR', $ignore_space);
    return $s->getStatement();
  }
  
  // SQL for TextFilter
  static function textFilter ($filter_id, &$form_state, $column, $case_sensitive = FALSE, $cast_numeric = FALSE, $ignore_space = TRUE) {
    $op = $form_state['values'][$filter_id . '_op'];
    $value = $form_state['values'][$filter_id];
    // numeric
    if (in_array($op, array('>', '=', '<'))) {
      if (is_numeric($value)) {
        $sql = "";
        if ($cast_numeric) {
          $sql = "(CAST($column AS NUMERIC) $op $value)";
        }
        else {
          $sql = "($column $op $value)";
        }
        return $sql;
      }
      else if (trim($value)) {
        form_set_error($filter_id,"'$value' is not a number.");
        return "(1 = 0)";
      }
      else {
        return NULL;
      }
    }
    // text
    else {      
      $s = new ColumnCond($column, $op, $value, $case_sensitive, NULL, 'OR', $ignore_space);
      return $s->getStatement();
    }
  }
  
  // SQL for TextareaFilter. $delimiter can not be a single quote '
  static function textareaFilter ($filter_id, &$form_state, $column, $case_sensitive = FALSE, $delimiter = "\n", $append_op = 'OR', $ignore_space = FALSE) {
    $op = $form_state['values'][$filter_id . '_op'];
    $values = $form_state['values'][$filter_id];
    $s = new ColumnCond($column, $op, $values, $case_sensitive, $delimiter, $append_op, $ignore_space);
    return $s->getStatement();
  }
  
  // SQL for TextFilter. Search the field on multiple columns
  // '$convert_to' will convert the matched columns into another column, typically an ID or primary key. It takes the format of '<column>:<table>'. Useful when the results were grouped.
  static function textFilterOnMultipleColumns ($filter_id, &$form_state, $columns = array(), $case_sensitive = FALSE, $convert_to = NULL, $ignore_space = TRUE) {
    $value = $form_state['values'][$filter_id];
    $op = $form_state['values'][$filter_id . '_op'];
    $s = new MultiColumnsCond($columns, $op, $value, $case_sensitive, $convert_to, $ignore_space);
    return $s->getStatement();
  }
  
  // SQL for TextFilter
  static function textFilterOnAggregateColumn ($filter_id, &$form_state, $column, $case_sensitive = FALSE, $delimiter = '::') {
    $op = $form_state['values'][$filter_id . '_op'];
    $value = $form_state['values'][$filter_id];
    $s = new AggregateColumnCond($column, $op, $value, $case_sensitive, $delimiter);
    return $s->getStatement();
  }
  
  // SQL for TextFilter
  static function selectFilterOnAggregateColumn ($filter_id, &$form_state, $column, $case_sensitive = FALSE, $delimiter = '::') {
    $op = 'exactly';
    $value = $form_state['values'][$filter_id];
    $s = new AggregateColumnCond($column, $op, $value, $case_sensitive, $delimiter);
    return $s->getStatement();
  }
  
  // SQL for FileFilter. Search the field on multiple columns
  static function fileOnMultipleColumns ($filter_id, $columns = array(), $case_sensitive = FALSE, $contains_word = FALSE, $convert_to = NULL, $limit = 0) {
    $file = $_FILES['files']['tmp_name'][$filter_id];
    $s = new FileMultiColumnsCond($columns, $file, $case_sensitive, $contains_word, $convert_to, $limit);
    return $s->getStatement();
  }
  
  // SQL for SelectFilter
  static function selectFilter ($filter_id, &$form_state, $column) {
    $value = $form_state['values'][$filter_id];
    $s = new SelectCond($column, $value);
    return $s->getStatement();
  }
  
  //SQL for BetweenFilter
  static function betweenFilter ($filter1_id, $filter2_id, &$form_state, $column1, $column2, $cast2real = FALSE) {
    $value1 = $form_state['values'][$filter1_id];
    $value2 = $form_state['values'][$filter2_id];
    $s = new BetweenCond($column1, $value1, $column2, $value2, $cast2real);
    return $s->getStatement();
  }
  
  // SQL for RepeatableText
  static function repeatableText($filter_id, $form_state) {
    // apply filters
    $filters = Sql::getRepeatableTextFilters($filter_id, $form_state);
    $s = new RepeatableTextCond($filters);
    return $s->getStatement();
  }
  
  // Get applied filters form $form_state['values']
  static function getRepeatableTextFilters ($filter_id, $form_state) {
    $values = $form_state['values'];
    $filters = array();
    foreach ($values AS $key => $value) {
      if (preg_match('/^' . $filter_id . '_\w+-\d+$/', $key)) {
        $token = explode('_', $key);
        $cond = explode('-', $token[1]);
        $id = $cond[1];
        $widget =$cond[0];
        $value = $form_state['values'][$key];
        if (!isset($filters[$id])) {
          $filters[$id] = array();
        }
        $filters[$id][$widget] = $value;
      }
    }
    
    foreach ($filters AS $k => $f) {
      // Remove filters that did not select a field
      if (isset($f['select']) && !$f['select']) {
        unset($filters[$k]);
      }
      // Remove filters that do not have any input text
      if (isset($f['text']) && trim($f['text'] == '')) {
        unset($filters[$k]);
      }
    }
    return $filters;
  }
  
  static function checkBoxColumnNotNull($filter_id, $form_sate) {
    $values = $form_sate['values'][$filter_id];
    $cols = array();
    foreach ($values AS $v) {
      if ($v) {
        $cols[] = $v;
      }
    }
    $stat = Sql::notNullCols($cols, 'AND');
    return $stat;
  }
  
  static function checkBoxColumnNotEmpty($filter_id, $form_sate) {
    $values = $form_sate['values'][$filter_id];
    $cols = array();
    foreach ($values AS $v) {
      if ($v) {
        $cols[] = $v;
      }
    }
    $stat = Sql::notEmptyCols($cols, 'AND');
    return $stat;
  }
  
  // SQL for File
  static function file ($filter_id, $column, $case_sensitive = FALSE, $contains_word = FALSE, $limit = 0) {
    $file = $_FILES['files']['tmp_name'][$filter_id];
    $s = new FileCond($file, $column, $case_sensitive, $contains_word, $limit);
    return $s->getStatement();
  }
  // Pair the SQL for two arrays that have the same elements by 'AND' or 'OR'.
  // Return an array with paired SQL conditions
  static function pairConditions ($arr1, $arr2, $concatbyOR = FALSE) {
    return Statement::pairConditions($arr1, $arr2, $concatbyOR);
  }
  
  // At least one of specified columns can not be NULL
  // Returns (col1 IS NOT NULL OR col2 IS NOT NULL OR col3 IS NOT NULL...)
  static function notNullCols ($columns, $op = 'OR') {
    return Statement::notNullCols($columns, $op);
  }
  
  static function notEmptyCols ($columns, $op = 'OR') {
    return Statement::notEmptyCols($columns, $op);
  }
  
  // Return a condition that make sure hstore has value for any of specified keys
  static function hstoreHasValue ($hs_col, $hs_keys = array()) {
    return Statement::hstoreHasValue($hs_col, $hs_keys);
  }
}