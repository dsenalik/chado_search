<?php

namespace ChadoSearch\sql;

// Create an SQL condition that filters the result on specified column that has aggreated values joining by :: delimiter
class AggregateColumnCond extends Statement {

  public function __construct($column, $op, $value, $case_sensitive, $delimiter = '::') {
    if (trim($value)) {
      $value = str_replace("'", "''", $value); // escape the single quote
      $this->statement = "(";
      if ($case_sensitive) {
        if ($op == 'contains') {
          $this->statement .= "$column like '%%" . $value . "%%'";
        } else if ($op == 'not_contain') {
            $this->statement .= "$column not like '%%" . $value . "%%'";
        } else if ($op == 'exactly') {
          $this->statement .= "($column = '$value') OR ($column like '%%$delimiter" . $value . "$delimiter%%') OR ($column like '" . $value . "$delimiter%%') OR ($column like '%%$delimiter" . $value . "')";
        } else if ($op == 'starts') {
          $this->statement .= "($column like '" . $value . "%%') OR ($column like '%%$delimiter" . $value . "%%') OR ($column like '" . $value . "%%')";
        } else if ($op == 'ends') {
          $this->statement .= "($column like '%%" . $value ."') OR ($column like '%%" . $value . "$delimiter%%') OR ($column like '" . $value . "$delimiter%%')";
        }
      }
      else {
        if ($op == 'contains') {
          $this->statement .= "lower($column) like lower('%%" . $value . "%%')";
        } else if ($op == 'not_contain') {
            $this->statement .= "$column not like '%%" . $value . "%%'";
        } else if ($op == 'exactly') {
          $this->statement .= "(lower($column) = lower('$value')) OR (lower($column) like lower('%%$delimiter" . $value . "$delimiter%%')) OR (lower($column) like lower('" . $value . "$delimiter%%')) OR (lower($column) like lower('%%$delimiter" . $value . "'))";
        } else if ($op == 'starts') {
          $this->statement .= "(lower($column) like lower('" . $value . "%%')) OR (lower($column) like lower('%%$delimiter" . $value . "%%')) OR (lower($column) like lower('" . $value . "%%'))";
        } else if ($op == 'ends') {
          $this->statement .= "(lower($column) like lower('%%" . $value ."')) OR (lower($column) like lower('%%" . $value . "$delimiter%%')) OR (lower($column) like lower('" . $value . "$delimiter%%'))";
        }
      }
      $this->statement .= ")";
    }
  }
}