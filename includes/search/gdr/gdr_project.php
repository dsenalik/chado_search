<?php

use ChadoSearch\Set;
use ChadoSearch\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_project_form ($form) {
  $form->addSelectFilter(
      Set::selectFilter()
      ->id('type')
      ->title('Project Type')
      ->column('type')
      ->table('chado_search_project')
      ->multiple(TRUE)
      ->labelWidth(100)
      ->newline()
  );
  $form->addSelectFilter(
    Set::selectFilter()
    ->id('sub_type')
    ->title('Sub Type')
    ->column('sub_type')
    ->table('chado_search_project')
      ->labelWidth(100)
    ->newline()
  );
  $form->addTextFilter(
      Set::textFilter()
      ->id('name')
      ->title('Name')
      ->labelWidth(100)
      ->newline()
   );
  $form->addTextFilter(
      Set::textFilter()
      ->id('description')
      ->title('Description')
      ->labelWidth(100)
      );
  $form->addSubmit();
  $form->addReset();
  $form->addFieldset(
      Set::fieldset()
      ->id('gene_search_fields')
      ->startWidget('type')
      ->endWidget('reset')
      );
  return $form;
}

// Submit the form
function chado_search_project_form_submit ($form, &$form_state) {
  // Get base sql
  $sql = "SELECT * FROM chado_search_project";
  $where = array();
  $where [] = Sql::selectFilter('type', $form_state, 'type');
  $where [] = Sql::selectFilter('sub_type', $form_state, 'sub_type');
  $where [] = Sql::textFilter('name', $form_state, 'name');
  $where [] = Sql::textFilter('description', $form_state, 'description');
  Set::result()
    ->sql($sql)
    ->where($where)
    ->tableDefinitionCallback('chado_search_project_table_definition')
    ->execute($form, $form_state);
}

/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_project_table_definition () {
  $headers = array(
      'name:s:chado_search_link_project:project_id' => 'Name',
      'type:s' => 'Type',
      'sub_type:s' => 'Sub Type',
      'description:s' => 'Description',
  );
  return $headers;
}
