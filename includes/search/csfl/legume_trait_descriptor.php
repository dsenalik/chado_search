<?php

use ChadoSearch\Set;
use ChadoSearch\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_trait_descriptor_form ($form) {
    $table = "
    (SELECT
      CV.name AS cv
    FROM cvterm C
    INNER JOIN cv CV on CV.cv_id = C.cv_id
    LEFT JOIN (
      SELECT (SELECT name FROM cv WHERE cv_id = C1.cv_id) AS cv, CR.subject_id
        FROM cvterm C1
        INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
        INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
        INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
        WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
    ) TRAIT on TRAIT.subject_id = C.cvterm_id
    WHERE TRAIT.CV LIKE '%_trait_ontology') T
  ";
    $form->addSelectFilter(
        Set::selectFilter()
        ->id('group')
        ->title('Group')
        ->table($table)
        ->column('cv')
        ->newline()
        );
    $cat = "
    (SELECT
        CAT.name AS category
      FROM cvterm C
      LEFT JOIN (
        SELECT (SELECT name FROM cv WHERE cv_id = C1.cv_id) AS cv, C1.name, C1.cvterm_id, CR.subject_id
          FROM cvterm C1
          INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
          INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
          INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
          WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
      ) TRAIT on TRAIT.subject_id = C.cvterm_id
      LEFT JOIN (
        SELECT C1.name, C1.cvterm_id, CR.subject_id
        FROM cvterm C1
        INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
        INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
        INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
        WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
      ) CAT on CAT.subject_id = TRAIT.cvterm_id
      WHERE TRAIT.cv LIKE '%_trait_ontology') T
  ";
    $form->addSelectFilter(
        Set::selectFilter()
        ->id('category')
        ->title('Category')
        ->table($cat)
        ->column('category')
        ->newline()
        );
    $form->addTextFilter(
        Set::textFilter()
        ->id('keyword')
        ->title('Keyword')
        ->newline()
        );
    $form->addSubmit();
    $form->addReset();
    return $form;
}

// Submit the form
function chado_search_trait_descriptor_form_submit ($form, &$form_state) {
    $where = array();
    // Get base sql
    $sql = chado_search_trait_descriptor_base_query();
    $where [] = Sql::selectFilter('group', $form_state, 'cv.name');
    $where [] = Sql::selectFilter('category', $form_state, 'cat.name');
    $where [] = Sql::textFilterOnMultipleColumns('keyword', $form_state, array('C.name', 'TRAIT.name', 'C.definition'));
    $where [] = "TRAIT.CV LIKE '%_trait_ontology'";
    Set::result()
    ->sql($sql)
    ->where($where)
    ->tableDefinitionCallback('chado_search_trait_descriptor_table_definition')
    ->execute($form, $form_state);
}

function chado_search_trait_descriptor_base_query () {
    $sql = "
    SELECT
      C.cvterm_id AS descriptor_id,
      CV.name AS cv,
      CAT.name AS category,
      TRAIT.cvterm_id AS trait_id,
      TRAIT.name AS trait,
      C.name AS descriptor,
      C.definition
    FROM cvterm C
    INNER JOIN cv CV on CV.cv_id = C.cv_id
    LEFT JOIN (
      SELECT (SELECT name FROM cv WHERE cv_id = C1.cv_id) AS cv, C1.name, C1.cvterm_id, CR.subject_id
        FROM cvterm C1
        INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
        INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
        INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
        WHERE CV2.name = 'MAIN' AND C2.name = 'belongs_to'
    ) TRAIT on TRAIT.subject_id = C.cvterm_id
    LEFT JOIN (
      SELECT C1.name, C1.cvterm_id, CR.subject_id
      FROM cvterm C1
      INNER JOIN cvterm_relationship CR on CR.object_id = C1.cvterm_id
      INNER JOIN cvterm C2 on C2.cvterm_id = CR.type_id
      INNER JOIN cv CV2 on CV2.cv_id = C2.cv_id
      WHERE CV2.name = 'relationship' AND C2.name = 'is_a'
    ) CAT on CAT.subject_id = TRAIT.cvterm_id
      ";
    return $sql;
}
/*************************************************************
 * Build the search result table
 */
// Define the result table
function chado_search_trait_descriptor_table_definition () {
    $headers = array(
        'cv:s' => 'Group',
        'descriptor:s:chado_search_trait_descriptor_link_descriptor:descriptor_id' => 'Descriptor',
        'category:s' => 'Category',
        'trait:s:chado_search_trait_descriptor_link_trait:trait_id' => 'Trait',
        'definition:s' => 'Definition'
    );
    return $headers;
}

// Define call back to link the trait to its  node for the result table
function chado_search_trait_descriptor_link_trait ($cvterm_id) {
    return "/trait/$cvterm_id";
}

function chado_search_trait_descriptor_link_descriptor ($cvterm_id) {
    return "/trait_descriptor/$cvterm_id";
}