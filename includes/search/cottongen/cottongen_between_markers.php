<?php

use ChadoSearch\Set;
use ChadoSearch\Sql;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_between_markers_form ($form) {
  $form->addTabs(
      Set::tab()
      ->id('between_marker_tabs')
      ->items(
          array(
            '/find/markers' => 'Marker Search',
            '/find/marker/source' => 'Marker Source',
            '/find/snp_markers' => 'SNP Marker Search',
            '/find/nearby_markers' => 'Nearby Loci',
            '/find/qtl_nearby_markers' => 'Nearby QTL',
            '/find/between_markers' => 'Between Markers',
          ))
      );
  
  $form->addLabeledFilter(
      Set::labeledFilter()
      ->id('between_marker_locus1')
      ->title('Locus1')
      ->newLine()
  );
  $form->addLabeledFilter(
      Set::labeledFilter()
      ->id('between_marker_locus2')
      ->title('Locus2')
  );
  
  $form->addSubmit();
  $form->addReset();
  $form->addFieldset(
      Set::fieldset()
      ->id('between_markers_fieldset')
      ->startWidget('between_marker_locus1')
      ->endWidget('reset')
      ->description("Search markers between two specific markers (eg: DPL0533 and MUSB1230)")
  );
  return $form;
}
// Validate the form
function chado_search_between_markers_form_validate ($form, &$form_state) {
  // Make sure both markers are not empty
  $locus1 = $form_state['values']['between_marker_locus1'];
  if (!$locus1) {
    form_set_error('', t('Locus1 is required.'));
  }
  $locus2 = $form_state['values']['between_marker_locus2'];
  if (!$locus2) {
    form_set_error('', t('Locus2 is required.'));
  }
  // Make sure both markers exist
  $exist1 = db_query("SELECT marker_feature_id FROM chado.chado_search_marker_search WHERE lower(marker_uniquename) = '" . strtolower($locus1) . "' LIMIT 1")->fetchField();
  if (!$exist1) {
    form_set_error('', t("Marer '" . $locus1 . "' does not exist."));
  }
  $exist2 = db_query("SELECT marker_feature_id FROM chado.chado_search_marker_search WHERE lower(marker_uniquename) = '" . strtolower($locus2) . "' LIMIT 1")->fetchField();
  if (!$exist2) {
    form_set_error('', t("Marker '" . $locus2 . "' does not exist."));
  }
}
// Submit the form
function chado_search_between_markers_form_submit ($form, &$form_state) {
  $locus1 = $form_state['values']['between_marker_locus1'];
  $locus2 = $form_state['values']['between_marker_locus2'];
  $lgs = array();
  $results = db_query("SELECT lg_name, min(start) AS min FROM chado.chado_search_marker_search WHERE lower(marker_uniquename) IN (:locus1, :locus2) GROUP BY lg_name", array(':locus1' => strtolower($locus1), ':locus2' => strtolower($locus2)));
  while ($m = $results->fetchObject()) {
    $lgs[$m->lg_name]['min'] = $m->min;
  }
  $results = db_query("SELECT lg_name, max(start) AS max FROM chado.chado_search_marker_search WHERE lower(marker_uniquename) IN (:locus1, :locus2) GROUP BY lg_name", array(':locus1' => strtolower($locus1), ':locus2' => strtolower($locus2)));
  while ($m = $results->fetchObject()) {
    $lgs[$m->lg_name]['max'] = $m->max;
  }

  // Get co-localized LGs
  // Make sure both markers co-exist on the same LG
  $co_lgs = array();
  $results = db_query(
      "SELECT DISTINCT lg_name
       FROM chado.chado_search_marker_search
       WHERE lg_name IN
         (SELECT DISTINCT lg_name FROM chado.chado_search_marker_search WHERE lower(marker_uniquename) = '" .  strtolower($locus1) . "')
       AND lower(marker_uniquename) = '" . strtolower($locus2) . "'");
  while ($l = $results->fetchObject()) {
    $co_lgs [$l->lg_name] = $l;
  }

  // Create conditions
  $compare = array();
  if (count($lgs) > 0) {
    foreach ($lgs AS $lg_name => $lg) {
      if ($lg['min'] != $lg['max'] && isset($co_lgs[$lg_name])) {
        $compare [$lg_name] = $lg;
      }
    }
  }

  $counter = 0;
  foreach ($compare AS $lg_name => $lg) {
    $where .= "(lg_name = '$lg_name' AND (start > " . $lg['min'] . " OR start = '" . $lg['min'] . "') AND " . " (start < " . $lg['max'] . " OR start = '" . $lg['max']. "'))";
    if ($counter < count($compare) - 1) {
      $where .= " OR ";
    }
    $counter ++;
  }
  if(!$where) {
    $where = array("1=0");
  }
  else {
    $where = array($where);
  }
  $sql = "SELECT DISTINCT marker_feature_id, marker_uniquename, marker_type, featuremap_id, map_name,lg_name, start FROM {chado_search_marker_search}";

  Set::result()
    ->sql($sql)
    ->where($where)
    ->defaultOrder('featuremap_id,lg_name,start')
    ->tableDefinitionCallback('chado_search_between_markers_table_definition')
    ->execute($form, $form_state);
}

/*************************************************************
 * Build the search result table
*/
// Define the result table
function chado_search_between_markers_table_definition () {
  $headers = array(      
    'marker_uniquename:s:chado_search_link_feature:marker_feature_id' => 'Name',
    'marker_type:s' => 'Type',
    'map_name:s:chado_search_link_featuremap:featuremap_id' => 'Map',
    'lg_name:s' => 'Linkage Group',
    'start:s' => 'Position',
  );
  return $headers;
}

/*************************************************************
 * AJAX callbacks
*/
// Downloading file ajax callback
function chado_search_between_markers_download_fasta_definition () {
  return 'marker_feature_id';
}
