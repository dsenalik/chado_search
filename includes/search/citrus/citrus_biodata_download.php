<?php
require_once 'citrus_biodata_download.mviews.php';

use ChadoSearch\Set;
use ChadoSearch\Sql;
use ChadoSearch\SessionVar;

use ChadoSearch\result\Download;
use ChadoSearch\result\Fasta;

/*************************************************************
 * Search form, form validation, and submit function
 */
// Search form
function chado_search_biodata_download_form ($form) {
  
  $form_state = $form->form_state;
  
  // Data Type
  $options = chado_search_get_all_mview_settings('chado_search_biodata_download_mview_definition', 'name');
  asort($options);
  $form->addSelectOptionFilter(
      Set::selectOptionFilter()
      ->id('datatype')
      ->title('Data Type')
      ->options($options)
      ->noKeyConversion(TRUE)
      ->newLine()
  );
  
  // Status & Counter
  $form->addDynamicMarkup(
    Set::dynamicMarkup()
      ->id('status')
      ->dependOnId('datatype')
      ->callback('chado_search_biodata_download_status')
      ->newline()
  );
  
  // Settings for the Filter & Attribute lists (hide if datatype not selected)
  $setttings_filter =
    Set::dynamicFieldset()
    ->id('data_filters')
    ->title('Query')
    ->dependOnId('datatype')
    ->width('70%');
  $setttings_attr =
    Set::dynamicFieldset()
    ->id('data_attributes')
    ->title('Download Fields')
    ->dependOnId('datatype')
    //->collapsed()
    ->width('27%');
    if(!isset($form_state['values']['datatype']) || $form_state['values']['datatype'] == '0') {
    $setttings_filter->display('none');
    $setttings_attr->display('none');
  }
    
  // Filter List fieldset
  $form->addDynamicFieldset($setttings_filter);
  
  // Attribute List fieldset
  $form->addDynamicFieldset($setttings_attr);
  
  if(isset($form_state['values']['datatype'])) {
    $mview = $form_state['values']['datatype'];
    $fields = chado_search_get_mview_setting('chado_search_biodata_download_mview_definition', $mview, 'field');
    if ($fields) {
      $ftypes = chado_search_get_field_types($mview, $fields);
      // Populate filter list
      $form->addRepeatableText(
          Set::repeatableText()
          ->id('conditions')
          ->items($fields)
          ->itemTypes($ftypes)
          ->fieldset('data_filters')
          ->maxlength(256)
          //->autoCompletePath('chado_search_autocomplete/' . $mview)
      );
      $form->addButton(
        Set::button()
        ->id('apply_filters')
        ->fieldset('data_filters')
        ->value('Refresh Count')
        ->ajax(array(
          'callback' => 'chado_search_biodata_download_ajax_apply_filters',
          'wrapper' => 'chado_search-filter-biodata_download-status-field',
          'effect' => 'fade')
        )
      );
      $form->addSubmit(
        Set::submit()
        ->value('Download')
        ->fieldset('data_filters')
      );
      
      // Fasta download if available
      $fasta = chado_search_get_mview_setting('chado_search_biodata_download_mview_definition', $mview, 'fasta');
      if ($fasta) {
        $form->addButton(
          Set::button()
          ->id('fasta_file')
          ->fieldset('data_filters')
          ->value('Create Fasta File')
        );
      }
      
      // Populate attribuet list
      $form->addCheckboxes(
        Set::checkBoxes()
          ->id('attribute_checkboxes')
          ->options($fields)
          ->fieldset('data_attributes')
          ->defaultValue(array_keys($fields))
      );
    }
  }

  // Reset Button
  $form->addReset();
  
  // Wrap form in Fieldset
  $form->addFieldset(
      Set::fieldset()
      ->id('biodata_download_fields')
      ->startWidget('datatype')
      ->endWidget('reset')
      ->description('Build your own query and download data in bulk. Select a data type to start. (Limit to 200,000 records/50,000 FASTA per download)')
  );
  return $form;
}



// Validate the form
function chado_search_biodata_download_form_validate ($form, &$form_state) {
  $limit = 200000;
  $limit_fasta = 50000;
  $form_state['rebuild'] = true;
  $values = $form_state['values'];
  $mview = $values['datatype'];  

  // Reset Repeatable Text if selecting a different datatype
  if(isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == 'chado_search-id-datatype') {
    $form_state['storage']['conditions-counter'] = 1;
    unset($form_state['values']['conditions_select-0']);
    unset($form_state['input']['conditions_select-0']);
    unset($form_state['values']['conditions_op-0']);
    unset($form_state['input']['conditions_op-0']);
    unset($form_state['values']['conditions_text-0']);
    unset($form_state['input']['conditions_text-0']);
  }
  // Create download file if Download button is clicked
  if (isset($form_state['clicked_button']['#id']) && $form_state['clicked_button']['#name'] == 'submit_button') {
    $mview = $values['datatype'];
    $cond = Sql::repeatableText('conditions', $form_state);
    $where = $cond ? ' WHERE ' . $cond : '';
    $sql ="SELECT * FROM {" . $mview . "}" . $where;
    SessionVar::setSessionVar('biodata_download','download', $sql);
    $sql_total = "SELECT count(*) FROM {" . $mview . "}" . $where;
    $total = chado_search_query($sql_total)->fetchField();
    // Make sure there is at least one result
    if ($total == 0) {
      form_set_error('data_filters', 'No result. Please adjust the query and try again');
      return;
    }
    else if ($total > $limit) {
      form_set_error('data_filters', 'The results exceed the limit of ' . number_format($limit) . ' records. Please add some filters and try again');
      return;
    }
    else {
      SessionVar::setSessionVar('biodata_download','total-items', $total);
    }
    // Limit download attributes
    $headers = chado_search_get_mview_setting('chado_search_biodata_download_mview_definition', $mview, 'field');
    $checkboxes = $values['attribute_checkboxes'];
    foreach ($checkboxes AS $key => $val) {
      if ($val == '0') {
        unset($headers[$key]);
      }
    }
    //Make sure at least one attribute is selected
    if (count($headers) == 0) {
      form_set_error('attribute_checkboxes', 'Nothing to download. Please select at least one field');
      return;
    }
    
    // If all checks passed, create the download file
    $dl = new Download('biodata_download', 'download/biodata', FALSE);
    $results = $dl->createDownload($headers);
    drupal_goto(preg_replace('/\?\d+$/', '',$results['path']));
  }

  // If Create Fasta File button is clicked
  if (isset($form_state['clicked_button']['#id']) && $form_state['clicked_button']['#id'] == 'chado_search-id-fasta_file') {
    $mview = $values['datatype'];
    $cond = Sql::repeatableText('conditions', $form_state);
    $where = $cond ? ' WHERE ' . $cond : '';
    $sql ="SELECT * FROM {" . $mview . "}" . $where;
    SessionVar::setSessionVar('biodata_download','sql', $sql);
    $sql_total = "SELECT count(*) FROM {" . $mview . "}" . $where;
    $total = chado_search_query($sql_total)->fetchField();
    if ($total == 0) {
      form_set_error('data_filters', 'No result. Please adjust the query and try again');
      return;
    }
    else if ($total > $limit_fasta) {
      form_set_error('data_filters', 'The results exceed the limit of ' . number_format($limit_fasta) . ' records for Fasta creation. Please add some filters and try again');
      return;
    }
    $dl = new Fasta('biodata_download', 'download/biodata');
    $results = $dl->createFasta();
    drupal_goto(preg_replace('/\?\d+$/', '',$results['path']));
  }
}

function chado_search_biodata_download_ajax_apply_filters ($form, $form_state) {
  $status = '';
  if (isset($form_state['clicked_button']['#id']) && $form_state['clicked_button']['#value'] == 'Refresh Count') {
    $mview = $form_state['values']['datatype'];
    $message = chado_search_biodata_download_status($mview, $form, $form_state);
    $status = $form['status'];
    $status['#markup'] = $message;
  }
  return $status;
}

// Update Status on the form with record counts
function chado_search_biodata_download_status($mview, $form, $form_state) {
  
  $message = NULL;
  if ($mview != '0') {
    $applyfilters = isset($form_state['triggering_element']['#id']) && $form_state['triggering_element']['#id'] == 'chado_search-id-apply_filters';
    if (chado_table_exists($mview)) {
      $sql_total = "SELECT count(*) FROM {" . $mview . "}";
      // apply filters
      $where = '';
      if ($applyfilters) {
        $cond = Sql::repeatableText('conditions', $form_state);
        $where = $cond ? ' WHERE ' . $cond : '';
      }
      // Run SQL
      $total = chado_search_query($sql_total . $where)->fetchField(); 
      SessionVar::setSessionVar('biodata_download','total-items', $total);
    }
    $message = '<strong>' . number_format($total) . '</strong> records';
  }
  return $message;
}
