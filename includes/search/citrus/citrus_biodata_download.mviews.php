<?php 
// Return the definition of the mview
function chado_search_biodata_download_mview_definition () {
  $def = array(
    'chado_search_featuremap' => array(
      'name' => 'Map',
      'field' => array(
        'featuremap' => 'Map Name',
        'organism' => 'Organism',
        'maternal_stock_uniquename' => 'Maternal Parent',
        'paternal_stock_uniquename' => 'Paternal Parent',
        'pop_size' => 'Population Size',
        'pop_type' => 'Population Type',
        'num_of_lg' => 'Number of LG',
        'num_of_loci' => 'Number of Loci'
      )
    ),
    'chado_search_gene_search' => array(
      'name' => 'Gene and Transcript',
      'field' => array(
        'name' => 'Name',
        'uniquename' => 'Unique Name',
        'genus' => 'Genus',
        'species' => 'Species',
        'organism' => 'Organism',
        'organism_common_name' => 'Organism Common Name',
        'feature_type' => 'Sequence Type',
        'landmark' => 'Chromosome/Scaffold',
        'fmin' => 'Start position',
        'fmax' => 'Stop position',
        'location' => 'Location',
        'analysis' => 'Genome',
        'blast_value' => 'BLAST',
        'kegg_value' => 'KEGG',
        'interpro_value' => 'InterPro',
        'go_term' => 'GO Term',
        'gb_keyword' => 'GenBank Keyword'
      ),
      'fasta' => 'feature_id'
    ),
    'chado_search_marker_search' => array(
      'name' => 'Marker',
      'field' => array(
        'marker_uniquename' => 'Unique Name',
        'marker_name' => 'Marker Name',
        'organism' => 'Organism',
        'map_name' => 'Map',
        'mapped_organism' => 'Mapped Organism',
        'lg_uniquename' => 'Linkage Group',
        'marker_type' => 'Marker Type',
        'start' => 'Map position (Start)',
        //'stop' => 'Map position (Stop)',
        'location' => 'Location',
        'synonym' => 'Synonym'
      ),
    ),
    'chado_search_qtl_search' => array(
      'name' => 'QTL',
      'field' => array(
        'qtl' => 'QTL Name',
        'organism' => 'Organism',
        'symbol' => 'Published Symbol',
        'trait' => 'Trait Name',
        'map' => 'Map',
        'col_marker_uniquename' => 'Colocolizing Marker',
        'neighbor_marker_uniquename' => 'Neighboring Marker',
        'study' => 'Study',
        'population' => 'Population',
        'lod' => 'LOD',
        'r2' => 'R2',
        'reference' => 'Reference',
        //'aliases' => 'Aliases',
        'type' => 'Type'
      )
    ),
    'chado_search_sequence_search' => array(
      'name' => 'Sequence',
      'field' => array(
        'name' => 'Sequence Name',
        'uniquename' => 'Unique Name',
        'feature_type' => 'Type',
        'genus' => 'Genus',
        'species' => 'Species',
        'organism' => 'Organism',
        'analysis_name' => 'Genome',
        'landmark' => 'Landmark',
        'fmin' => 'Start position',
        'fmax' => 'Stop position',
        'location' => 'Location'
      ),
      'fasta' => 'feature_id'
    ),
    'chado_search_germplasm_search' => array(
      'name' => 'Germplasm',
      'field' => array(
        'name' => 'Stock Name',
        'uniquename' => 'Unique Name',
        'organism' => 'Organism',
        'genome' => 'Genome',
        'alias' => 'Alias'
      )
    ),
    'chado_search_species' => array(
      'name' => 'Organism',
      'field' => array(
        'genus' => 'Genus',
        'species' => 'Species',
        'common_name' => 'Common Name',
        'grin' => 'GRIN',
        'geographic_origin' => 'Geographic Origin',
        'num_germplasm' => 'Number of Germplasm',
        'num_sequences' => 'Number of Sequences',
        'num_libraries' => 'Number of Libraries'
      )
    ),
    'pub' => array(
      'name' => 'Publication',
      'field' => array(
        'title' => 'Title',
        'series_name' => 'Journal Name',
        'issue' => 'Issue',
        'pyear' => 'Year',
        'pages' => 'Pages',
        'uniquename' => 'Citation & Authors'
      )
    ),
    'project' => array(
      'name' => 'Project',
      'field' => array(
        'name' => 'Name',
        'description' => 'Description'
      )
    )
  );
  return $def;
}