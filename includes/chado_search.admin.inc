<?php
/**
 * Launch page for chado_search administration.
 *
 */
function chado_search_admin_form() {
    
    $form = array();
    $searches = chado_search_get_all_searches (FALSE);
    $access = variable_get('chado_search_access', array());
    $headers = array('Title', 'ID', 'URL', 'PHP', 'Enabled', 'Action', 'Access');
    $rows = array();
    foreach ($searches AS $search) {
        $rows[] = array(
            $search['title'],
            $search['id'],
            $search['enabled'] ? l($search['path'], $search['path']) : $search['path'],
            $search['file'],
            $search['enabled'] ? 'Yes' : 'No',
            $search['enabled'] ? l('Disable', '/admin/mainlab/chado_search/action/disable/' . $search['id']) : l('Enable', '/admin/mainlab/chado_search/action/enable/' . $search['id']),
            key_exists($search['id'], $access) ? l('Restricted by Roles', '/admin/mainlab/chado_search/action/allow/' . $search['id']) : l('All Granted', '/admin/mainlab/chado_search/action/restrict/' . $search['id'])
        );
    }
    if (count($rows) == 0) {
        $rows[] = array('No search available.', array('colspan' => 6));
    }
    
    $table = array(
        'header' => $headers,
        'rows' => $rows,
        'attributes' => array(),
        'sticky' => FALSE,
        'caption' => '',
        'colgroups' => array(),
        'empty' => '',
    );
    $output = theme_table($table);
    
    $form['instructions'] = array(
        '#type' => 'item',
        '#markup' => t('List of configured search interfaces')
    );
    $form['table'] = array(
        '#markup' => $output
    );
    $access_roles = variable_get('chado_search_access_roles', array());
    $roles = user_roles ();
    $form['restriction'] = array(
        '#type' => 'fieldset',
        '#title' => 'Restrict Access',
        '#description' => "Effective only when access is switched from 'All Granted' to 'Restricted by Roles' in above search listing table",
        '#collapsible' => TRUE
    );
    $form['restriction']['access'] = array(
      '#type' => 'checkboxes',
      '#title' => 'Roles',
      '#description' => "Allow access only for the selected roles.",
      '#options' => $roles,
      '#default_value' => $access_roles
    );
    $access_denied_message = variable_get('chado_search_access_denied_message', 'You are not authorized to use this seach function.');
    $form['restriction']['access_denied_message'] = array(
        '#type' => 'textarea',
        '#title' => 'Access Denied Message',
        '#description' => 'Message shown to the user when access is denied.',
        '#default_value' => $access_denied_message,
        '#size' => 150
    );
    $form['download'] = array(
        '#type' => 'fieldset',
        '#title' => 'Download File',
        '#collapsible' => TRUE
    );
    $download_file_header = variable_get('chado_search_download_file_header', '');
    $form['download']['download_file_header'] = array(
        '#type' => 'textarea',
        '#title' => 'Download File Header',
        '#description' => 'Add a header message to all download files. Default: no header message.',
        '#default_value' => $download_file_header,
        '#size' => 150
    );
    
    $hb_search_enabled = chado_search_get_setting_by_id ('haplotype_block_search', 'enabled');
    if($hb_search_enabled && db_table_exists('chado.chado_search_haplotype_block_marker_location') && db_table_exists('chado.chado_search_haplotype_block_marker_position')) {
        $sql = "SELECT DISTINCT project_id, project FROM chado.chado_search_haplotype_block_search";
        $result = db_query($sql);
        $projects = array();
        while ($obj = $result->fetchObject()) {
            $projects[$obj->project_id] = $obj->project;
        }
        $sql = "SELECT DISTINCT genome_analysis_id, genome FROM chado.chado_search_haplotype_block_marker_location";
        $result = db_query($sql);
        $genomes = array();
        while ($obj = $result->fetchObject()) {
            $genomes[$obj->genome_analysis_id] = $obj->genome;
        }
        $sql = "SELECT DISTINCT featuremap_id, map FROM chado.chado_search_haplotype_block_marker_position";
        $result = db_query($sql);
        $maps = array();
        while ($obj = $result->fetchObject()) {
            $maps[$obj->featuremap_id] = $obj->map;
        }
        $form['haplotype_block_search'] = array(
            '#title' => t('Haplotype Block Search Settings'),
            '#description' => t('The following are settings specific to the Haplotype Block Search'),
            '#type' => 'fieldset',
            '#collapsible' => TRUE
        );
        $default_genomes = variable_get('chado_search_haplotype_search_genomes', array());
        $default_maps = variable_get('chado_search_haplotype_search_maps', array());

        foreach ($projects AS $project_id => $project) {
            $form['haplotype_block_search'][$project_id] = array(
                '#title' => $project,
                '#type' => 'fieldset',
                '#collapsible' => TRUE,
                '#collapsed' => TRUE
            );
            $form['haplotype_block_search'][$project_id]['genomes_' . $project_id] = array(
                '#title' => t('Genomes for showing marker genome location'),
                '#description' => t('Select genome(s) for showing marker genome location. Only locations on selected genomes will be displayed. If a marker is on more than one selected genome, only one of them will be shown.'),
                '#type' => 'checkboxes',
                '#options' => $genomes,
                '#default_value' => $default_genomes[$project_id]
            );
            $form['haplotype_block_search'][$project_id]['maps_' . $project_id] = array(
                '#title' => t('Maps for showing marker map position'),
                '#description' => t('Select map(s) for showing marker map position. Only positions on selected maps will be displayed. If a marker is on more than one selected map, only one of them will be shown.'),
                '#type' => 'checkboxes',
                '#options' => $maps,
                '#default_value' => $default_maps[$project_id]
            );
        }
    }
    return system_settings_form($form);
}

/**
 * chado_search form validation.
 *
 */
function chado_search_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  $access = $values['access'];
  $access_roles = array();
  foreach ($access AS $k => $v) {
      if ($v > 0) {
          $access_roles[$k] = $v;
      }
  }
  variable_set('chado_search_access_roles', $access_roles);
  $access_denied_message = $values['access_denied_message'];
  variable_set('chado_search_access_denied_message', $access_denied_message);
  
  $download_file_header = $values['download_file_header'];
  variable_set('chado_search_download_file_header', trim($download_file_header));
  
  $var_genome_ids = array();
  $var_map_ids = array();

  foreach ($values AS $key => $value) {
      if (preg_match('/genomes_/', $key)) {
          $pkey = explode('_', $key);
          foreach ($value AS $k => $v) {
            if ($v > 0) {
              $var_genome_ids [$pkey[1]][] = $k;
            }
          }
      }
      else if (preg_match('/maps_/', $key)) {
          foreach ($value AS $k => $v) {
            if ($v > 0) {
                $var_map_ids [$pkey[1]][] = $k;
            }
          }
      }
  }
  variable_set('chado_search_haplotype_search_genomes', $var_genome_ids);
  variable_set('chado_search_haplotype_search_maps', $var_map_ids);
}

function chado_search_admin_action($action, $search_id) {
  if ($action == 'enable') {
    $succeed = chado_search_set_setting_by_id($search_id, 'enabled=1');
  }
  else if ($action == 'disable'){
    $succeed = chado_search_set_setting_by_id($search_id, 'enabled=0');
  }
  else if ($action == 'restrict'){
      $access = variable_get('chado_search_access', array());
      $access [$search_id] = $search_id;
      variable_set('chado_search_access', $access);
  }
  else if ($action == 'allow'){
      $access = variable_get('chado_search_access', array());
      unset($access [$search_id]);
      variable_set('chado_search_access', $access);
  }
  if ($succeed) {
      menu_rebuild();
  }
  drupal_goto('/admin/mainlab/chado_search/settings');
}