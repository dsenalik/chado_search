# Csform API document v1.0
For creating a custom search form, see 'Create New Search' section in the README.md to learn the steps.
This document lists all widgets that can be added to a custom search form. A custom search form object
is passed in from the 'hook_search_form ($form)' function. To add a new widget onto the form, call
$form->addWidget() function and set up the widget by calling Set::widget() with desired configurations
as shown in the following paradigm:
  ```
    $form->addWidget(
      Set::widget()
      ->configuration1()
      ->configuration2()
      ...
    )
  ```
Note 1. Configurations are orderless and mostly optional except for the ->id('unique_id'). Examples are shown below.

Note 2. The custom search $form object is not the same as the Drupal Form API's $form array.

Listed below are two types of widgets, 'Combo filters' (used more often) and 'Basic form elements' with example configurations.

# Combo filters (i.e. combination of basic form elements)

1. BetweenFilter (Two sets of LabeledFilter)
  ```
    $form->addBetweenFilter(
      Set::betweenFilter()
      ->id('unique_id')
      ->id('unique_id')
      ->title('some_title')
      ->title('some_title')
      ->label_width(100)
      ->label_width(120)
      ->size(20)
      ->newline()
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

2. LabeledFilter (A markup label and a textfield)
  ```
    $form->addLabeledFilter(
      Set::labeledFilter()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->size(20)
      ->label_width(100)
      ->newline()
      ->maxlength(256)
      ->autocomplete_path('/chado_search_autocomplete/feature/name')
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

3. TextFilter (A markup label, a select with values (contains/exactly/starts with/ends with/dose not contain), and a textfield)
  ```
    $form->addTextFilter(
      Set::textFilter()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->label_width(100)
      ->size(20)
      ->maxlength(128)
      ->newline()
      ->autocomplete_path('/chado_search_autocomplete/feature/name')
      ->fieldset_id('add_to_this_fieldset')
      ->numeric()
    )
  ```

4. TextareaFilter (A markup label, a select with values (contains/exactly/starts with/ends with/dose not contain), and a textarea)
  ```
    $form->addTextareaFilter(
      Set::textareaFilter()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->label_width(100)
      ->cols()
      ->rows()
      ->newline()
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

5. SelectFilter (A markup label and a select). The available values are retrieved from database column and table.
   The available values are the distinct values of specified column in specified table.
   Multiple columns are allowed by passing an array of columns for which values are
   concatenated by a comma (,).
   Common selection values can be passed in as an array to the $optgroup variable.
   Alternatively, the selection values can be grouped by passing in a $optgroup_by_pattern
   variable consisting of array('Display Group' => 'pattern').
   If $cache is TRUE, a cacahe table will be created and this will greatly improve the
   performance for rendering the search form. The cache will automatically refresh when
   new data are added and you will never need to clear it by hand.
  ```
    $form->addSelectFilter(
      Set::selectFilter()
      ->id('unique_id')
      ->title('some_title')
      ->column('name')
      ->table('feature')
      ->condition('feature_id > 100')
      ->required(FALSE)
      ->multiple(TRUE)
      ->column_natural_sort(TRUE)
      ->optgroup(array('frequently','used','options'))
      ->optgroup_by_pattern(array('prunus' => 'Peach Genome', 'malus' => 'Apple Genome'))
      ->cache(TRUE)
      ->label_width(100)
      ->size(20)
      ->newline()
      ->disables(array('disable', 'these', 'options'))
      ->only(array('show', 'only', 'these', 'options'))
      ->search_box(TRUE)
      ->nullable(TRUE)
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

6. SelectOptionsFilter (A markup label and a select). The available values are passed in as an array().
  ```
    $form->addSelectOptionsFilter(
      Set::selectOptionsFilter()
      ->id('unique_id')
      ->title('some_title')
      ->options()
      ->required(FALSE)
      ->multiple(TRUE)
      ->nokeyconversion()
      ->label_width(100)
      ->newline()
      ->size(20)
      ->fieldset_id('add_to_this_fieldset')
      ->ajax = array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
    )
  ```

7. DynamicMarkup (A markup whose value was derived from a select element). A $value variable will be passing into your AJAX function
  ```
    $form->addDynamicMarkup(
      Set::dynamicMarkup()
      ->id('unique_id')
      ->depend_on_id('a_SelectFilter_id');
      ->callback('come_callback')
      ->newline()
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

8. DynamicFieldset (A fieldset displayed when a value is choosen from a select box)
  ```
    $form->addDynamicFieldset(
      Set::hidden()
      ->id('unique_id')
      ->depend_on_id();
      ->title('some_title')
      ->description('some_description')
      ->collapsible(TRUE)
      ->collapsed(FALSE)
      ->width(400)
      ->display('block')
      ->newline()
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

9. DynamicSelectFilter (A computed select whose value was derived from another select element). A $value variable will be passing into your AJAX function
  ```
    $form->addDynamicSelectFilter(
      Set::dynamicSelectFilter()
      ->id('unique_id')
      ->title('some_title')
      ->depend_on_id('another_SelectFilter_id')
      ->callback('some_callback')
      ->newline()
      ->label_width(100)
      ->cacheTable('some_table')
      ->cacheColumns('some_column')
      ->reset_on_change_id('another_SelectFilter_id')
      ->multiple(TRUE)
      ->alsoDependOn('another_SelectFilter_id')
      ->hidden(FALSE)
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

10. DynamicTextFields. Upon select status changes, a couple of text fields can be populated with values. A $value variable will be passing into your AJAX function
  ```
    $form->addDynamicTextFields(
      Set::dynamicTextFields()
      ->id('unique_id')
      ->target_ids('another_SelectFilter_id')
      ->callback('some_callback')
      ->newline()
      ->reset_on_change_id('another_SelectFilter_id')
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

11. Fieldset (Grouping widgets into a fieldset)
  ```
    $form->addDynamicTextFields(
      Set::hidden()
      ->id('unique_id')
      ->title('some_title')
      ->start_widget('groupping_start_widget_id')
      ->end_widget('groupping_end_widget_id')
      ->desc('some_description')
      ->collapased(FALSE)
      ->newline()
      ->clearboth(TRUE)
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

12. Tabs (Horizontal tabs with hyperlink)
  ```
   $form->addTabs(
    Set::tabs()
      ->id('unique_id')
      ->items(array('path1' => 'Item1', 'path2' => 'Item2'))
      ->newline()
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

13. SelectShortCut (hyperlink for quick selection on a Select box)
  ```
    $form->addSelectShortCut(
      Set::selectShortCut(
      ->id('unique_id')
      ->selectbox_id('another_SelectFilter_id')
      ->value('select_this_value')
      ->pretext('some_text')
      ->postext('some_text')
      ->newline()
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

14. CustomOutput. (Expose output fields to the users)
  ```
    $form->addCustomOutput(
      Set::customOutput()
      ->id('unique_id')
      ->options(array('option1', 'option2', 'option3'))
      ->defaults('option1')
      ->title('some_title')
      ->desc('some_description')
      ->collapsible(TRUE)
      ->collapsed(FALSE)
      ->group_selection= (TRUE);
      ->max_columns(TRUE)
      ->row_counter(TRUE)
      ->fieldset_id('add_to_this_fieldset')
    )
  ```

15. RepeatableText (A select, a textfield, and a repeat button that will add another set of filter widgets upon clicked)
  ```
    $form->addRepeatableText(
      Set::repeatableText()
      ->id('unique_id')
      ->required(FALSE)
      ->size(20)
      ->newline()
      ->fieldset_id('add_to_this_fieldset')
      ->items(array('name', 'uniquename'))
      ->itemTypes(array('varchar', 'text'))
      ->maxlength()
      ->autocomplete_path('/chado_search_autocomplete/feature/name')
    )
  ```

# Basic form elements (corresponds to Drupal's form elements)

1. Hidden (a hidden value that passed along when the form is submitted)
  ```
    $form->addHidden(
      Set::hidden()
      ->id('unique_id')
      ->value('some_value')
      ->default_value('some_value')
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```

2. TextField
  ```
    $form->addTextField(
      Set::textField()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->size(100)
      ->maxlength(256)
      ->default_value('some_value')
      ->display('block')
      ->fieldset_id('add_to_this_fieldset')
      ->autocomplete_path('/chado_search_autocomplete/feature/name')
      ->newline())
  ```

3. TextArea
  ```
    $form->addTextArea(
      Set::textArea()
      ->id('unique_id')
      ->title('some_title')
      ->required(FALSE)
      ->cols(20)
      ->rows(10)
      ->default_value('some_value')
      ->display('block')
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```

4. Select (a drop-down with options)
  ```
    $form->addSelect(
      Set::textSelect()
      ->id('unique_id')
      ->title('some_title')
      ->options(array('key1' => 'option1', 'key2' => 'option2'))
      ->multiple(FALSE)
      ->size(100)
      ->default_value(array('key1'))
      ->display('block')
      ->fieldset_id('add_to_this_fieldset')
      ->ajax(
        array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
      ->newline())
  ```

5. Markup (HTML markup)
  ```
    $form->addMarkup(
      Set::markup()
      ->id('unique_id')
      ->markup('some_markup')
      ->display('block')
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```

6. File
  ```
    $form->addFile(
      Set::file()
      ->id('unique_id')
      ->label('some_label')
      ->instruction('some_instruction')
      ->size(20)
      ->labelWidth(100)
      ->display('block')
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```

7. Checkboxes
  ```
    $form->addCheckboxes(
      Set::checkboxes()
      ->id('unique_id')
      ->title('some_title')
      ->options(array('key1' => 'option1', 'key2' => 'option2'))
      ->default_value(array('key1'))
      ->display('block')
      ->fieldset_id('add_to_this_fieldset')
      ->ajax(
        array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
      ->newline())
  ```

8. Button
  ```
    $form->addButton(
      Set::button()
      ->id('unique_id')
      ->value('Submit')
      ->name('some_name')
      ->button_type('button')
      ->attributes(array('onMouseDown' => 'some_js_code'))
      ->display('block')
      ->ajax(
        array(
          'callback' => 'some_callback'',
          'wrapper' => 'some_css_id',
          'effect' => 'fade'
        )
      )
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```

9. ClearButton (a clear form button)
  ```
    $form->addClearButton(
      Set::clearButton()
      ->id('unique_id')
      ->value('Clear')
      ->name('some_name')
      ->button_type('button')
      ->attributes(array('onMouseDown' => 'some_js_code'))
      ->display('block')
      ->ajax(
        array(
          'callback' => 'chado_search_ajax_form_clear_values'',
          'wrapper' => 'chado_search_form',
          'effect' => 'fade'
        )
      )
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```

10. Submit (a submit button)
  ```
    $form->addSubmit(
      Set::submit()
      ->id('unique_id')
      ->value('Submit')
      ->name('some_name')
      ->attributes(array('onMouseDown' => 'some_js_code'))
      ->ajax(
        array(
          'callback' => 'chado_search_ajax_form_clear_values'',
          'wrapper' => 'chado_search_form',
          'effect' => 'fade'
        )
      )
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```

11. Reset (a reset button)
  ```
    $form->addReset(
      Set::reset()
      ->id('unique_id')
      ->path('goto_path')
      ->newline())
  ```

12. Throbber (a throbber progress indicator)
  ```
    $form->addThrobber(
      Set::throbber()
      ->id('unique_id')
      ->fieldset_id('add_to_this_fieldset')
      ->newline())
  ```